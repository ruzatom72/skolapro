package com.example.mealplanner.controller;

import com.example.mealplanner.model.Recipe;
import com.example.mealplanner.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RecipeController {

    @Autowired
    private RecipeService recipeService;

    @GetMapping("/recipes")
    public String listRecipes(Model model) {
        model.addAttribute("recipes", recipeService.findAll());
        return "recipes";
    }

    @GetMapping("/recipes/new")
    public String newRecipe(Model model) {
        model.addAttribute("recipe", new Recipe());
        return "recipe_form";
    }

    @PostMapping("/recipes")
    public String saveRecipe(Recipe recipe) {
        recipeService.save(recipe);
        return "redirect:/recipes";
    }
}
