package com.example.mealplanner.controller;

import com.example.mealplanner.model.MealPlan;
import com.example.mealplanner.service.MealPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MealPlanController {

    @Autowired
    private MealPlanService mealPlanService;

    @GetMapping("/mealplans")
    public String listMealPlans(Model model) {
        model.addAttribute("mealplans", mealPlanService.findAll());
        return "mealplans";
    }

    @GetMapping("/mealplans/new")
    public String newMealPlan(Model model) {
        model.addAttribute("mealplan", new MealPlan());
        return "mealplan_form";
    }

    @PostMapping("/mealplans")
    public String saveMealPlan(MealPlan mealPlan) {
        mealPlanService.save(mealPlan);
        return "redirect:/mealplans";
    }
}
