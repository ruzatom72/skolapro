package com.example.mealplanner.repository;

import com.example.mealplanner.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
