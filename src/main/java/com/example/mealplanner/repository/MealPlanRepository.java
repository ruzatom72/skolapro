package com.example.mealplanner.repository;

import com.example.mealplanner.model.MealPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MealPlanRepository extends JpaRepository<MealPlan, Long> {}
