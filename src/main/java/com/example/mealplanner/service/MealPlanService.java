package com.example.mealplanner.service;

import com.example.mealplanner.model.MealPlan;
import com.example.mealplanner.repository.MealPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MealPlanService {
    @Autowired
    private MealPlanRepository mealPlanRepository;

    public List<MealPlan> findAll() {
        return mealPlanRepository.findAll();
    }

    public MealPlan save(MealPlan mealPlan) {
        return mealPlanRepository.save(mealPlan);
    }

    public MealPlan findById(Long id) {
        return mealPlanRepository.findById(id).orElse(null);
    }
}
