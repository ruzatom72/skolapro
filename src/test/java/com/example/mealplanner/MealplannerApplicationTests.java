package com.example.mealplanner;

import com.example.mealplanner.model.MealPlan;
import com.example.mealplanner.service.MealPlanService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class MealServiceTests {
    @Autowired
    private MealService mealService;

    @Test
    public void testFindAllMeals() {
        List<MealPlan> meals = mealService.findAll();
        assertNotNull(meals);
    }


}
